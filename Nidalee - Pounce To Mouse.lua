Menu = scriptConfig("Pounce To Mouse", "Nidalee")
Menu:addParam("ptm", "Pounce To Mouse", SCRIPT_PARAM_ONKEYDOWN, false, 32)
print("Pounce To Mouse Loaded - Sleepwalker (AKA Pain)")
function OnTick()
	if myHero:GetSpellData(_Q).name == "Takedown" and Menu.ptm then
		alpha = math.atan(math.abs(mousePos.z-myHero.z)/math.abs(mousePos.x-myHero.x))
        locX = math.cos(alpha)*(GetDistance(mousePos) - (GetDistance(mousePos)-300))
        locZ = math.sin(alpha)*(GetDistance(mousePos) - (GetDistance(mousePos)-300))
        myHero:MoveTo(math.sign(mousePos.x-myHero.x)*locX+myHero.x, math.sign(mousePos.z-myHero.z)*locZ+myHero.z)
		DelayAction(Pounce,0.15)
	end
end
function Pounce()
if myHero:GetSpellData(_Q).name == "Takedown" then
	CastSpell(_W)
end
end
function math.sign(x)
 if x < 0 then
  return -1
 elseif x > 0 then
  return 1
 else
  return 0
 end
end