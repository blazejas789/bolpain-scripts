function OnLoad()
	PreviousEmote = 0
	PrintChat("<font color='#6e3c99'>Loaded Emote Spammer for, The Insane.</font>")
	PrintChat("<font color='#6e3c99'>0 = Laugh, 1 = Taunt, 2 = Joke. These can be changed in the menu.</font>")
	Menu = scriptConfig("Emote Spammer", "Emote Spam")
	Menu:addParam("Spam", "Emote Spam", SCRIPT_PARAM_ONKEYDOWN, false, string.byte(" "))
	Menu:addParam("Emote", "Emote To Spam", SCRIPT_PARAM_SLICE, 2, 0, 2, 0) 
	Menu:permaShow("Spam")
end
function OnTick()
	if Menu.Spam then
		player:MoveTo(mousePos.x,mousePos.z)
		if GetTickCount() - PreviousEmote > 5000 then
		if Menu.Emote == 0 then SendChat("/l")
			PreviousEmote = GetTickCount() 			
		end
		if Menu.Emote == 1 then SendChat("/t")
			PreviousEmote = GetTickCount() 
		end
		if Menu.Emote == 2 then SendChat("/j")
			PreviousEmote = GetTickCount() 
		end
		end
	end
end