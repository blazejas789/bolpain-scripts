--[[

OUTDATED:

Features:

	Sida's Auto Carry, Nidalee.lua	
	
        - Mixed Mode:
            - Uses Spears to harras.
        
        - Lane Clean:
            - Use E in Cougar form.
            
        - Auto Carry Mode:
            - In Human Form:
				- Throws Spear at target.
				- Places Trap at target.
				- Changes form if user cannot cast Q.
			-  In Cougar form:
				- Uses Leap to jump to target.
				- Uses E for AOE and extra damage.
				- Uses Q when target is below desired percentage of health (Look in shift menu).

				
        - Shift Menu:
			- General
				- Human Form
					- Auto Heal.
						- Will heal allies if user's mana is above desired percentage (Look in shift menu). Will heal allies if ally's health is below desired percentage.
						
		
			- Mixed Mode
				- Spear Harras
					- If toggled on then the script will use spears to harras in mixed mode.
				
			- Lane Clean Mode
				- Use E
					- If toggled on then the script will use E to clear the lane faster whilst in Cougar form.
					
			- Auto Carry Mode
				- Uses Full Combo (Cannot toggle).

    Version 1.0
    - Public Release
	
	Version 1.1
	- Added E Ready check (Forgot about it)
    
    Instructions on saving the file:
    - Save the file as:
        - SidasAutoCarryPlugin - Nidalee.lua
		
--]]

function PluginOnLoad()
SkillQ = {spellKey = _Q, range = 1400, speed = 1.3, delay = 0.150, width = 90, minions = true}
SkillW = {spellKey = _W, range = 900, speed = 1.3, delay = 0.150, width = 90}
Menu = AutoCarry.PluginMenu
AutoCarry.SkillsCrosshair.range = 1450
NidaMenu()
PrintChat("Great thanks to HeX for helping me with the plugin.")
end

function PluginOnTick()

if Menu.HealAll then Healing() end
FormCheck()
ReadyChecks()
SpellRanges()
	if Target then
	if AutoCarry.MainMenu.AutoCarry then
		if HUMAN == true then
			HumanCombo()
		end
		if COUGAR == true then
			CougarCombo()
		end
	end
	
	if AutoCarry.MainMenu.MixedMode then
		if HUMAN == true then
			if QREADY and Menu.QHarras then
				AutoCarry.CastSkillshot(SkillQ, Target)
			end
		end
	end
	end
	if AutoCarry.MainMenu.LaneClear then
		if COUGAR == true then
			CougarFarm()
		end
	end
end

function CougarFarm()
	if Menu.UseE then
		for _, minion in pairs(AutoCarry.EnemyMinions().objects) do
			if ValidTarget(minion) and GetDistance(minion) <= eRange then
                CastSpell(_E)
			end
        end
	end
end

function HumanCombo()
	if QREADY then
		AutoCarry.CastSkillshot(SkillQ, Target)
	end
	if WREADY then
		AutoCarry.CastSkillshot(SkillW, Target)
	end
	
	if not QREADY then
		CastSpell(_R)
	end
end

function CougarCombo()
	if QREADY then
		if (Target.health / Target.maxHealth) < Menu.QHealth then
			CastSpell(_Q)
		end
	end
	
	if WREADY and GetDistance(Target) < wRange then
		CastSpell(_W)
	end
	
	if EREADY and GetDistance(Target) < eRange then
		CastSpell(_E)
	end
end

function Healing()
	if myHero.mana >= myHero.maxMana*Menu.HealMana then
	
	if HUMAN == true then
		for i=1, heroManager.iCount do
			local allytarget = heroManager:GetHero(i)
			if allytarget.team == myHero.team and not allytarget.dead then 
				if GetDistance(allytarget) <= 600 and allytarget.health <= allytarget.maxHealth*Menu.HealHealth then
					if myHero:CanUseSpell(_E) == READY then 
						CastSpell(_E, allytarget)
						if COUGAR == true and Menu.ForceHeal then
								CastSpell(_R)
							end
					end
				end
			end
		end
	end
		

	end
end

function NidaMenu()
Menu:addParam("sep", "-- Skills --", SCRIPT_PARAM_INFO, "")
Menu:addParam("space1", "", SCRIPT_PARAM_INFO, "")
Menu:addParam("sep1", "  -- Human Form --", SCRIPT_PARAM_INFO, "")
Menu:addParam("space2", "", SCRIPT_PARAM_INFO, "")
Menu:addParam("sep4", "    -- Mixed Mode --", SCRIPT_PARAM_INFO, "")
Menu:addParam("QHarras", "Use Spears to Harras", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("sep5", "      -- Auto Carry --", SCRIPT_PARAM_INFO, "")
Menu:addParam("sep6", "Will use Q and W in Combo", SCRIPT_PARAM_INFO, "")
Menu:addParam("space3", "", SCRIPT_PARAM_INFO, "")
Menu:addParam("sep3", "    -- Healing --", SCRIPT_PARAM_INFO, "")
Menu:addParam("HealAll", "Heals Allies if below X Health", SCRIPT_PARAM_ONKEYTOGGLE, true, string.byte("K"))
Menu:addParam("HealHealth", "X Amount of Health to heal", SCRIPT_PARAM_SLICE, 0.6, 0.1, 0.9, 1)
Menu:addParam("HealMana", "X Amount of Mana to heal", SCRIPT_PARAM_SLICE, 0.4, 0.1, 1, 1)
Menu:addParam("ForceHeal", "Change form to heal", SCRIPT_PARAM_ONOFF, true)

Menu:addParam("space4", "", SCRIPT_PARAM_INFO, "")
Menu:addParam("space5", "", SCRIPT_PARAM_INFO, "")
Menu:addParam("sep2", "  -- Cougar Form--", SCRIPT_PARAM_INFO, "")
Menu:addParam("QHealth", "% of enemy health to cast Q", SCRIPT_PARAM_SLICE, 0.7, 0.1, 1, 1)
Menu:addParam("UseE", "Use E to farm with Lane Clear", SCRIPT_PARAM_ONOFF, true)
end

function FormCheck()
    if myHero:GetSpellData(_Q).name == "JavelinToss" or myHero:GetSpellData(_W).name == "Bushwhack" or myHero:GetSpellData(_E).name == "PrimalSurge" then
        HUMAN = true
		COUGAR = false
		AutoCarry.SkillsCrosshair.range = 1450
    end
	if myHero:GetSpellData(_Q).name == "Takedown" or myHero:GetSpellData(_W).name == "Pounce" or myHero:GetSpellData(_E).name == "Swipe" then
        COUGAR = true
		HUMAN = false
		AutoCarry.SkillsCrosshair.range = wRange
    end
end

function SpellRanges()
	if HUMAN == true then
	SkillQ = {spellKey = _Q, range = 1400, speed = 1.3, delay = 0.150, width = 90, minions = true}
	SkillW = {spellKey = _W, range = 900, speed = 1.3, delay = 0.150, width = 90}
	end
	
	if COUGAR == true then
	qRange = getMyTrueRange()
	wRange = 600
	eRange = 280
	end
end

function ReadyChecks()
        Target = AutoCarry.GetAttackTarget()
        QREADY = (myHero:CanUseSpell(_Q) == READY )
        WREADY = (myHero:CanUseSpell(_W) == READY )
		EREADY = (myHero:CanUseSpell(_E) == READY )
        RREADY = (myHero:CanUseSpell(_R) == READY ) 
end

function getMyTrueRange()
	return myHero.range + GetDistance(myHero, myHero.minBBox)
end