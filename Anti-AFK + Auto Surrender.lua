local delay = 0
local Mdelay = 0
local Mtdelay = (Mdelay + 0)

local p50 = (myHero.x + 150)
local m50 = (myHero.x - 150)
local pz50 = (myHero.z + 150)
local mz50 = (myHero.z - 150)

Menu = scriptConfig("Surrender", "Surrender")
Menu:addParam("FF", "Surrender Toggled :", SCRIPT_PARAM_ONOFF, false)
Menu:addParam("AAFK", "Anti AFK Toggled :", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("FFT", "Delay between surrenders (Min) :", SCRIPT_PARAM_SLICE, 1, 1, 20, 0)
Menu:addParam("AAFKT", "Delay between movement (Sec) :", SCRIPT_PARAM_SLICE, 1, 1, 60, 0)

function OnTick()
TimesDelay = (Menu.FFT * 60000)
MoveDelay1 = (Menu.AAFKT * 1000)
MoveDelay2 = (Menu.AAFKT * 2000)
if GetTickCount() > Mdelay + MoveDelay1 then
if Menu.AAFK then
myHero:MoveTo(p50, pz50)
end
Mdelay = GetTickCount()
end

if GetTickCount() > Mtdelay + MoveDelay2 then
if Menu.AAFK then
myHero:MoveTo(m50, mz50)
end
Mtdelay = GetTickCount()
end
if GetTickCount() > delay + TimesDelay then
if Menu.FF then
SendChat("/ff")
end
delay = GetTickCount()
end
end