--[[
		Vol..OMG IT'S A BEAR!
		
		SidasAutoCarryPlugin - Volibear.lua
		
		- Features:
		
			- MAN MODE
			
				- Volibear talks.. Like a human.. What?
			
			- BEAR MODE:
			
				- Auto KS.
				
				- Full combo, customizable Q range (Edit within the shift menu)
				
				- Ranges drawn with circles around Volibear.
				
				- Shift Menu:
					
					- Use [KEY] in Combo.
						- Will use said spell in the Combo.
						
					- Distance between "user" and Target to use Q.
						- This is where you set up your q Range, there is a default range of Auto Attack range + 400.
					
					- Kill 'SECURE'
						- Will use W to secure the kill.
						
					- Save W for the Kill
						- Won't use W in the combo and will wait until the Target is low enough to die.
						
					- Draw Circles
						- Will draw the ranges of Volibear's spells around Volibear.
			
			- PIG MODE
				
				- The Pig form has yet to come.. We've never seen the pig form before.
]]

function Menu()
    Menu = AutoCarry.PluginMenu
	Menu2 = AutoCarry.MainMenu
	Menu:addParam("Plugin", "[Volibear Plugin Options]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("UseQ", "Use Q in Combo", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("UseW", "Use W in Combo", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("UseE", "Use E in Combo", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("UseR", "Use R in Combo", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("MiscInfo", "[Volibear Misc Options]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("ChargeQINFO", "Distance between me and Target to use Q", SCRIPT_PARAM_INFO, "")
	Menu:addParam("ChargeQ", "(Range indicated with Circle)", SCRIPT_PARAM_SLICE, 400, 0, 800, 0)
	Menu:addParam("AutoKS", "Kill 'SECURE' with W", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("SaveW", "Save W for kill if Enabled Auto KS", SCRIPT_PARAM_ONOFF, false)
	Menu:addParam("DrawInfo", "[Volibear OnDraw Options]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("Draw", "Draw Circles", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DrawQ", "Draw Circles for Q", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DrawW", "Draw Circles for W", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DrawE", "Draw Circles for E", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DrawR", "Draw Circles for R", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DrawT", "Draw Circles around Target", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("QuickDraw", "Show the Mini-Menu", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("QuickDrawINFO", "Show the Mini-Menu Requires a refresh (F9, F9)", SCRIPT_PARAM_INFO, "")
end
function PluginOnLoad()
	Menu()
	if Menu.QuickDraw then
	Menu:permaShow("Plugin")
	Menu:permaShow("ChargeQINFO")
	Menu:permaShow("ChargeQ")
	Menu:permaShow("AutoKS")
	Menu:permaShow("SaveW")
	end
end
function getMyTrueRange()
	return myHero.range + GetDistance(myHero, myHero.minBBox)
end
function PluginOnTick()
	if not myHero.dead then	
	Target = AutoCarry.GetAttackTarget()
	QREADY = (myHero:CanUseSpell(_Q) == READY)
    WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
    RREADY = (myHero:CanUseSpell(_R) == READY)
	qRange = getMyTrueRange() + Menu.ChargeQ
	wRange = 350
	eRange = 410
	rRange = getMyTrueRange()
	AutoCarry.SkillsCrosshair.range = (qRange + 100)
	if Menu.AutoKS then
		if Target and WREADY then
			local BITEDamage1 = getDmg("W",Target,myHero)
			if (BITEDamage1 > Target.health) and (GetDistance(Target,myHero) < wRange) then
				CastSpell(_W, Target)
			end
		end
	end
	if Menu2.AutoCarry and Target then
		if GetDistance(Target,myHero) < qRange and Menu.UseQ and QREADY then
			CastSpell(_Q)
		end
		if GetDistance(Target,myHero) < wRange and Menu.UseW and WREADY and not Menu.SaveW then
			CastSpell(_W, Target)
		end
		if GetDistance(Target,myHero) < eRange and Menu.UseE and EREADY then
			CastSpell(_E)
		end
	end
	end
end
function OnAttacked()
	if Menu.UseR and Target and Menu2.AutoCarry and GetDistance(Target,myHero) < rRange then
		CastSpell(_R)
	end
end
function PluginOnDraw()
	if Menu.Draw and not myHero.dead then
		qRange = getMyTrueRange() + Menu.ChargeQ
		wRange = 350
		eRange = 425
		rRange = getMyTrueRange()
		if Menu.DrawQ then
		DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0x7F006E)
		end
		if Menu.DrawW then
		DrawCircle(myHero.x, myHero.y, myHero.z, wRange, 0x960084)
		end
		if Menu.DrawE then
		DrawCircle(myHero.x, myHero.y, myHero.z, eRange, 0x490041)
		end
		if Menu.DrawR then
		DrawCircle(myHero.x, myHero.y, myHero.z, rRange, 0xC900B1)
		end
		if Menu.DrawT and Target then
		DrawCircle(Target.x, Target.y, Target.z, 100, 0x7F006E)
		end
	end
end