if myHero.charName ~= "Morgana" then return end
function PluginOnLoad()
	qRange = 1275
	wRange = 900
	eRange = 750
	AutoCarry.SkillsCrosshair.range = qRange
    Cast = AutoCarry.CastSkillshot
    Menu = AutoCarry.PluginMenu
	Menu2 = AutoCarry.MainMenu
	MorganaMenu()
	
	rRange = Menu.rRange
	qWidth = Menu.qWidth
	SkillQ = {spellKey = _Q, range = qRange, speed = 1.2, delay = 250, width = qWidth, minions = true}
	SkillW = {spellKey = _W, range = wRange, speed = 1.2, delay = 250, width = 105}
end
function PluginOnTick()
    Target = AutoCarry.GetAttackTarget()
    QREADY = (myHero:CanUseSpell(_Q) == READY )
    WREADY = (myHero:CanUseSpell(_W) == READY )
	EREADY = (myHero:CanUseSpell(_E) == READY )
    RREADY = (myHero:CanUseSpell(_R) == READY )
	if Target then
	if Menu2.AutoCarry then
		if Menu.AutoQ and QREADY then
			Cast(SkillQ, Target)
		end
		if Menu.AutoW and Menu.StunW and not Target.canMove and WREADY then
			CastSpell(_W, Target.x, Target.z)
		end
		if Menu.AutoW and not Menu.StunW and WREADY then
			Cast(SkillW, Target)
		end
		autoR()
	end
	end
end
function autoR()
	if RREADY and Menu.AutoR then
		if CountEnemyHeroInRange(Menu.rRange) >= Menu.rCount then
				CastSpell(_R)
		end
	end
end
function MorganaMenu()
Menu:addParam("sep", "[Cast Options]", SCRIPT_PARAM_INFO, "")
Menu:addParam("AutoQ", "Auto Cast Q", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("qWidth", "Width of Q ", SCRIPT_PARAM_SLICE, 70, 55, 100, 0)
Menu:addParam("sep4", "Don't touch 'Width of Q' unless, ", SCRIPT_PARAM_INFO, "")
Menu:addParam("sep5", "you know what you're doing", SCRIPT_PARAM_INFO, "")
Menu:addParam("gap1", "", SCRIPT_PARAM_INFO, "")
Menu:addParam("sep2", "[Soil Cast Options]", SCRIPT_PARAM_INFO, "")
Menu:addParam("AutoW", "Auto Cast W", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("StunW", "Wait for Q then W", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("gap2", "", SCRIPT_PARAM_INFO, "")
Menu:addParam("sepE", "[Ultimate Cast Options]", SCRIPT_PARAM_INFO, "")
Menu:addParam("AutoR", "Auto Cast R", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("rRange", "Range to cast R: ", SCRIPT_PARAM_SLICE, 500, 50, 600, 0)
Menu:addParam("rCount", "Min Enemies to cast R: ", SCRIPT_PARAM_SLICE, 2, 1, 5, 0)
end
function PluginOnProcessSpell(unit, spell)
if unit.isMe and spell.name == myHero:GetSpellData(_Q).name then
   -- print("<font color='#21007F'>Cast Spell Q</font>")
		local LastCast = Q
end
if unit.isMe and spell.name == myHero:GetSpellData(_W).name then
   -- print("<font color='#57007F'>Cast Spell W</font>")
		local LastCast = W
end
if unit.isMe and spell.name == myHero:GetSpellData(_E).name then
  --  print("<font color='#7F006E'>Cast Spell E</font>")
		local LastCast = E
end
if unit.isMe and spell.name == myHero:GetSpellData(_R).name then
   -- print("<font color='#7F006E'>Cast Spell R</font>")
		local LastCast = R
end
end