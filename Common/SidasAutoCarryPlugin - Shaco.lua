local SkillW = {spellKey = _W, range = 420, speed = 1.6, delay = 325}
Target = AutoCarry.GetAttackTarget()
AutoCarry.SkillsCrosshair.range = 630
--[[TargetPos = Vector(Target.x, Target.y, Target.z)
MyPos = Vector(myHero.x, myHero.y, myHero.z)
BackPos = TargetPos + (TargetPos-MyPos)*((50/GetDistance(Target)))]]
Menu = AutoCarry.MainMenu
Menu2 = AutoCarry.PluginMenu
Menu2:addParam("sep", "[Cast Options]", SCRIPT_PARAM_INFO, "")
Menu2:addParam("DrawR", "Draw Circles for Clone", SCRIPT_PARAM_ONOFF, true)
Menu2:addParam("UseQ", "Use Q in Combo", SCRIPT_PARAM_ONOFF, true)
Menu2:addParam("UseW", "Use W in Combo", SCRIPT_PARAM_ONOFF, true)
Menu2:addParam("UseE", "Use E in Combo", SCRIPT_PARAM_ONOFF, true)
 TestColour = nil
function PluginOnTick()
if shacoClone ~= nil then
if GetDistance(shacoClone) < 2418 and GetDistance(shacoClone) > 1725 then
TestColour = 0xFF0000
end
if GetDistance(shacoClone) < 1724 and GetDistance(shacoClone) > 1250 then
TestColour = 0xE88A0E
end
if GetDistance(shacoClone) < 1249 and GetDistance(shacoClone) > 0 then
TestColour = 0x00CE00
end
end
if Target then
    if Menu.AutoCarry or Menu.MixedMode then
		if Menu2.UseQ then
        QCast()
		end
		if Menu2.UseW then
        WCast()
		end
		if Menu2.UseE then
        ECast()
		end
    end
	end
end
function QCast()
		if myHero:CanUseSpell(_Q) == READY and GetDistance(Target) < 400 then
			CastSpell(_Q, Target.x, Target.z)
	end
end
function WCast()
        if GetDistance(Target) < 425 and myHero:CanUseSpell(_W) == READY then
            AutoCarry.CastSkillshot(SkillW, Target)
    end
end
function ECast()
        if GetDistance(Target) < 625 and myHero:CanUseSpell(_E) == READY then
            CastSpell(_E, Target)
    end
end
function PluginOnCreateObj(object)
	if object ~= nil and object.name:find("Jester_Copy") then
		shacoClone = object
    end
end
function PluginOnDeleteObj(object)
	if object ~= nil and object.name:find("Jester_Copy") then
        shacoClone = nil
    end
end
function PluginOnDraw()
if Menu2.DrawR then
    if not myHero.dead then
        if shacoClone ~= nil then
			DrawCircle(myHero.x, myHero.y, myHero.z, 250, TestColour)
            DrawCircle(myHero.x, myHero.y, myHero.z, 2418, TestColour)
        end
    end
end
end