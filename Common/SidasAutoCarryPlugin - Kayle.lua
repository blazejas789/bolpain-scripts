--[[

	Sida's Auto Carry, Kayle Plugin.

	All Credits to HeX for the original plugin.
	
	Features:
		- Mixed Mode:
			- Auto Carry Mode
		
		- Last Hit:
			- N/A.
		
		- Lane Clean:
			- Uses E to clear lanes at a safe distance (AOE Damage too).
			
		- Auto Carry Mode:
			- Uses Q and W automatically.
			- Smartly uses E to speed up if out of range of the desired target or to self heal if target is in the range. (Check Shift Menu)
			
		- Shift Menu:
			- Use Q
			- Use W
			- Use E
				- If any of the Use "KEY" is toggled on then it will use the desired spell in the combo (Auto Carry mode).
				
		- Auto Heal.
			- Will heal allies if user's mana is above desired percentage (Look in shift menu). Will heal allies if ally's health is below desired percentage.
						

	Version 1.0
	- Public Release
	
	Instructions on saving the file:
	- Save the file as:
		- SidasAutoCarryPlugin - Kayle.lua

--]]


function PluginOnLoad()
        mainLoad()
        mainMenu()
		PrintChat("Credits to HeX for the original Kayle Plugin")
end
 
function PluginOnTick()
        Checks()
		if Menu.HealAll then Healing() end
		if Menu2.LaneClear then
			for _, minion in pairs(AutoCarry.EnemyMinions().objects) do
					if ValidTarget(minion) and GetDistance(minion) <= eRange and EREADY then
						CastSpell(_E)
					end
				end
		end
        if Target and (Menu2.AutoCarry or Menu2.MixedMode) then
                if EREADY and Menu.useE and GetDistance(Target) < eRange then
                        CastSpell(_E)
                end
                if QREADY and Menu.useQ and GetDistance(Target) < qRange then
                        CastSpell(_Q, Target)
                end
                if WREADY and Menu.useW and GetDistance(Target) < wBuffer then
						if (myHero.health / myHero.maxHealth) < Menu.HealHealth then
							CastSpell(_W)
						end
                end
				if WREADY and Menu.useW and GetDistance(Target) > wBuffer then
					CastSpell(_W)
				end
        end
end
 
function PluginOnDraw()
        if Menu.drawQ and not myHero.dead then
                DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0xFF80FF00)
        end
end
 
--> Checks
function Checks()
        Target = AutoCarry.GetAttackTarget()
        QREADY = (myHero:CanUseSpell(_Q) == READY)
        WREADY = (myHero:CanUseSpell(_W) == READY)
        EREADY = (myHero:CanUseSpell(_E) == READY)
end
 
--> Main Load
function mainLoad()
        AutoCarry.SkillsCrosshair.range = 1050
        qRange, wBuffer, eRange = 650, 650, 525
        QREADY, WREADY, EREADY = false, false, false
        Menu = AutoCarry.PluginMenu
        Menu2 = AutoCarry.MainMenu
end
 
function mainMenu()
        Menu:addParam("sep", "-- Cast Options --", SCRIPT_PARAM_INFO, "")
        Menu:addParam("useQ", "Use - Reckoning", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("useW", "Use - Divine Blessing", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("useE", "Use - Righteous Fury", SCRIPT_PARAM_ONOFF, true)
        Menu:addParam("sep1", "-- Draw Options --", SCRIPT_PARAM_INFO, "")
        Menu:addParam("drawQ", "Draw - Reckoning", SCRIPT_PARAM_ONOFF, false)
		Menu:addParam("sep3", "-- Healing --", SCRIPT_PARAM_INFO, "")
		Menu:addParam("HealAll", "Heals Allies if below X Health", SCRIPT_PARAM_ONKEYTOGGLE, true, string.byte("K"))
		Menu:addParam("HealHealth", "X Amount of Health to heal", SCRIPT_PARAM_SLICE, 0.6, 0.1, 0.9, 1)
		Menu:addParam("HealMana", "X Amount of Mana to heal", SCRIPT_PARAM_SLICE, 0.4, 0.1, 1, 1)
end

function Healing()
	if myHero.mana >= myHero.maxMana*Menu.HealMana then
	if HUMAN == true then
		for i=1, heroManager.iCount do
			local allytarget = heroManager:GetHero(i)
			if allytarget.team == myHero.team and not allytarget.dead then 
				if GetDistance(allytarget) <= 900 and (allytarget.health / allytarget.maxHealth) < Menu.HealHealth then
					if myHero:CanUseSpell(_E) == READY then 
						CastSpell(_E, allytarget)
						if COUGAR == true and Menu.ForceHeal then
								CastSpell(_R)
							end
					end
				end
			end
		end
	end
		

	end
end