function Menu()
Menu = AutoCarry.PluginMenu
Menu2 = AutoCarry.MainMenu
Menu:addParam("sep", "[Cast Options]", SCRIPT_PARAM_INFO, "")
Menu:addParam("sep2", "[Human Options]", SCRIPT_PARAM_INFO, "")
Menu:addParam("AutoQ", "Use Q in Combo/Harass mode", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("AutoR", "Use R when you cannot use Q", SCRIPT_PARAM_ONKEYTOGGLE, true, string.byte("A"))
Menu:addParam("sep3", "[Cougar Options]", SCRIPT_PARAM_INFO, "")
Menu:addParam("AutoCQ", "Use Q in Combo", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("QPerc", "Percent of Target's HP to use Q", SCRIPT_PARAM_SLICE, 0.85, 0.1, 1, 3)
Menu:addParam("AutoCW", "Use W in Combo", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("AutoCE", "Use E in Combo", SCRIPT_PARAM_ONOFF, true)
Menu:addParam("AutoCR", "Use R when Spear is ready", SCRIPT_PARAM_ONKEYTOGGLE, true, string.byte("T"))
Menu:addParam("sep4", "[Misc Options]", SCRIPT_PARAM_INFO, "")
Menu:addParam("DrawC", "Draw Circles", SCRIPT_PARAM_ONOFF, true)
--Menu:addParam("sep4", "[Misc Options]", SCRIPT_PARAM_INFO, "")
--Menu:addParam("Draw", "Enable OnDraw Functions", SCRIPT_PARAM_ONOFF, true)
end
function PluginOnLoad()
	AutoCarry.SkillsCrosshair.range = 1450
    Cast = AutoCarry.CastSkillshot
	Menu()
	SkillQ = {spellKey = _Q, range = 1400, speed = 1.3, delay = 0.150, width = 95, minions = true}
end
Nidalee = { ["JavelinToss"] = 0, }
function PluginOnTick()
		--Checks--
		if myHero:GetSpellData(_Q).name == "JavelinToss" then
			NidaleeForm = true
		end
		if myHero:GetSpellData(_Q).name == "Takedown" then
			NidaleeForm = false
		end
		QREADY = (myHero:CanUseSpell(_Q) == READY)
		WREADY = (myHero:CanUseSpell(_W) == READY)
		EREADY = (myHero:CanUseSpell(_E) == READY)
		RREADY = (myHero:CanUseSpell(_R) == READY)
		Target = AutoCarry.GetAttackTarget()
		--End of Checks--
		for i = _Q, _Q do
			local spell = myHero:GetSpellData(i)
			if NidaleeForm then
				if Nidalee["JavelinToss"] and Nidalee["JavelinToss"] < GetInGameTimer() and spell.currentCd > 0 then
					Nidalee["JavelinToss"] = GetInGameTimer() + spell.currentCd          -- you save the time in seconds, when the spell is going to be ready
				end
			end
		end
		Q_cd = math.max(0, Nidalee["JavelinToss"] - GetInGameTimer())
		NidaleeCombo()
end
function AutoPounce()
	if WREADY and NidaleeForm == false then
		AutoCarry.CanMove = true
		CastSpell(_W)
	end
end
function AutoSwipe()
	if EREADY and NidaleeForm == false then
		AutoCarry.CanMove = true
		CastSpell(_E)
	end
end
function OnAttacked()
	-- Takedown --
	if NidaleeForm == false then
		if QREADY and Menu.AutoCQ and Menu2.AutoCarry and GetDistance(Target,myHero) < 250 then
			if (Target.health / Target.maxHealth) < Menu.QPerc then
				CastSpell(_Q)
			end
		end
	end
		-- End of Takedown --
end
function NidaleeCombo()
	if not myHero.dead and Target ~= nil then
		-- Human Form --
		if NidaleeForm == true then
		--Q Cast--
		if QREADY and (Menu2.AutoCarry or Menu2.MixedMode) and Menu.AutoQ then
			Cast(SkillQ, Target)
		end
		--End of Q Cast--
		-- Swap Forms --
		if Menu2.AutoCarry and not QREADY then
			CastSpell(_R)
		end
		-- End of Swap Forms--
		end
		-- End of Human Form --
		-- Cougar Form --
		if NidaleeForm == false then
		-- Pounce --
		if WREADY and Menu.AutoCW and Menu2.AutoCarry and GetDistance(Target,myHero) < 600 then
			AutoCarry.CanMove = false
			myHero:MoveTo(Target.x, Target.z)
			DelayAction(AutoPounce,0.125)
		end
		-- End of Pounce -- 
		-- Swipe --
		if EREADY and Menu.AutoCE and Menu2.AutoCarry and GetDistance(Target,myHero) < 275 then
			AutoCarry.CanMove = false
			myHero:MoveTo(Target.x, Target.z)
			DelayAction(AutoSwipe,0.125)
		end
		-- End of Swipe --
		-- Swap Forms --
		if RREADY and Menu2.AutoCarry and Menu.AutoCR then
			if (Q_cd < 0.3) then CastSpell(_R) end
		end
		-- End of Swap Forms --
		end
		-- End of Cougar Form --
	end
end
function PluginOnDraw()
	if Menu.DrawC then
		if NidaleeForm == true then
			DrawCircle(myHero.x, myHero.y, myHero.z, 1450, 0x7F006E)
		end
	end
end