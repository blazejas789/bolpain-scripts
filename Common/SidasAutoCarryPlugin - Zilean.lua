--[[

	Sida's Auto Carry, Zilean Plugin.

	
	Features:
		- Mixed Mode:
			- Uses Q, W, Q combo to harras.
		
		- Last Hit & Lane Clean:
			- N/A
			
		- Auto Carry Mode:
			- Uses Q, W, Q combo (If you have enough mana)
			- Uses E on self if Target is out of range.
			- Uses E on Target if Target is in range.
			- Uses Q if not enough mana to cast the Q, W, Q combo.
			
		- Shift Menu:
			- Nothing Here :(

	Version 1.0
	- Public Release
	
	Instructions on saving the file:
	- Save the file as:
		- SidasAutoCarryPlugin - Zilean.lua
	
--]]

function PluginOnLoad()
Menu = AutoCarry.PluginMenu
AutoCarry.SkillsCrosshair.range = 900
NilSkill()
ZileanMenu()
end
function PluginOnTick()
LevelCheck()
ReadyChecks()
ManaChecks()
	if Target then
			if AutoCarry.MainMenu.AutoCarry or AutoCarry.MainMenu.MixedMode then
				if ((qMana + qMana) + wMana) < myHero.mana then
					if GetDistance(Target) < 690 and QREADY then
						CastSpell(_Q, Target)
						CastSpell(_W)
					end
				end
			end
			if AutoCarry.MainMenu.AutoCarry then
				if qMana < myHero.mana and QREADY then
					CastSpell(_Q, Target)
				end
				if eMana < myHero.mana and EREADY and GetDistance(Target) > 700 then
					CastSpell(_E, myHero)
				end
				if eMana < myHero.mana and EREADY and GetDistance(Target) < 700 then
					CastSpell(_E, Target)
				end
				
			end
	end
end
function NilSkill()
qMana = nil
wMana = nil
eMana = nil
Q1, Q2, Q3, Q4 = nil, nil, nil, nil
W1, W2, W3, W4 = nil, nil, nil, nil
E1, E2, E3, E4 = nil, nil, nil, nil
end
function ManaChecks()
if Q1 then qMana = 70 end
if Q2 then qMana = 85 Q1 = false end
if Q3 then qMana = 100 Q2 = false end
if Q4 then qMana = 115 Q3 = false end
if Q5 then qMana = 130 Q4 = false end
if W1 then wMana = 70 end
if W2 then wMana = 85 W1 = false end
if W3 then wMana = 100 W2 = false end
if W4 then wMana = 115 W3 = false end
if W5 then wMana = 130 W4 = false end
if E1 then eMana = 70 end
if E2 then eMana = 85 E1 = false end
if E3 then eMana = 100 E2 = false end
if E4 then eMana = 115 E3 = false end
if E5 then eMana = 130 E4 = false end
end
function LevelCheck()
if myHero:GetSpellData(_Q).level == 1 then Q1 = true end
if myHero:GetSpellData(_Q).level == 2 then Q1 = true end
if myHero:GetSpellData(_Q).level == 3 then Q1 = true end
if myHero:GetSpellData(_Q).level == 4 then Q1 = true end
if myHero:GetSpellData(_Q).level == 5 then Q1 = true end
if myHero:GetSpellData(_W).level == 1 then W1 = true end
if myHero:GetSpellData(_W).level == 2 then W2 = true end
if myHero:GetSpellData(_W).level == 3 then W3 = true end
if myHero:GetSpellData(_W).level == 4 then W4 = true end
if myHero:GetSpellData(_W).level == 5 then W5 = true end
if myHero:GetSpellData(_E).level == 1 then E1 = true end
if myHero:GetSpellData(_E).level == 2 then E2 = true end
if myHero:GetSpellData(_E).level == 3 then E3 = true end
if myHero:GetSpellData(_E).level == 4 then E4 = true end
if myHero:GetSpellData(_E).level == 5 then E5 = true end
end
-- If you've read it this far then here is your reward.. http://puu.sh/4d3vh.png -- 
function ReadyChecks()
    Target = AutoCarry.GetAttackTarget()
    QREADY = (myHero:CanUseSpell(_Q) == READY )
    WREADY = (myHero:CanUseSpell(_W) == READY )
	EREADY = (myHero:CanUseSpell(_E) == READY )
    RREADY = (myHero:CanUseSpell(_R) == READY ) 
end
function ZileanMenu()
Menu:addParam("sep", "-- No options available --", SCRIPT_PARAM_INFO, "")
end