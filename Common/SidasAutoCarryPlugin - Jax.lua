--[[
	SidasAutoCarryPlugin - Jax
	
	Features:
		Only Auto Carry Mode:
			- Use E before Q (Toggle with T)
				- Casts E before Q so you can stun the target quicker.
			
			- Use [KEY] in Combo
				- Use [KEY] whilst you hold the Auto Carry button.
				
		Misc:
			- Draw Circles:
				- Draws circles to the respected ranges.
				
	Changelog:
		- Version 1.0
			- Release
			
	Notes: Hey, do you even read the features?
]]

function Menu()
    Menu = AutoCarry.PluginMenu
	Menu2 = AutoCarry.MainMenu
	Menu:addParam("Plugin", "[Jax Plugin Options]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("UseQ", "Use Q in Combo", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("UseW", "Use Q in Combo", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("UseE", "Use Q in Combo", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("UseR", "Use Q in Combo", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("UseEtQ", "Use E before Q", SCRIPT_PARAM_ONKEYTOGGLE, false, string.byte("T"))
	Menu:addParam("DrawInfo", "[OnDraw Options]", SCRIPT_PARAM_INFO, "")
	Menu:addParam("Draw", "Draw Circles", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DrawQ", "Draw Circles for Q", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DrawW", "Draw Circles for W", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DrawE", "Draw Circles for E", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DrawR", "Draw Circles for R", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("DrawT", "Draw Circles around Target", SCRIPT_PARAM_ONOFF, true)
	Menu:permaShow("Plugin")
	Menu:permaShow("UseEtQ")
end

function PluginOnLoad()
	qRange = 700
	wRange = getMyTrueRange()
	eRange = 180
	rRange = getMyTrueRange()
	Menu()
	CounterStrike = false
end
function PluginOnTick()
	Target = AutoCarry.GetAttackTarget()
	QREADY = (myHero:CanUseSpell(_Q) == READY)
    WREADY = (myHero:CanUseSpell(_W) == READY)
	EREADY = (myHero:CanUseSpell(_E) == READY)
    RREADY = (myHero:CanUseSpell(_R) == READY)
	if QREADY then AutoCarry.SkillsCrosshair.range = qRange end
	if not QREADY then AutoCarry.SkillsCrosshair.range = 350 end
	if Target and Menu2.AutoCarry then
		if Menu.UseEtQ then
			if GetDistance(Target,myHero) < qRange and Menu.UseE then
				if CounterStrike == false and EREADY and QREADY then
					CastSpell(_E)
				end
				if CounterStrike == true and QREADY and Menu.UseQ then
					CastSpell(_Q, Target)
				end
				if CounterStrike == true and GetDistance(Target,myHero) < eRange and Menu.UseE then
					CastSpell(_E)
				end
			end
		else
			if Menu.UseE then
				if GetDistance(Target,myHero) < eRange then
					CastSpell(_E)
				end
			end
			if Menu.UseQ then
				if GetDistance(Target,myHero) < qRange then
					CastSpell(_Q, Target)
				end
			end
		end
		
		if Menu.UseR then
			if GetDistance(Target,myHero) < rRange then
				CastSpell(_R)
			end
		end
	end
end
function OnAttacked()
	if Menu.UseW and Menu2.AutoCarry then
		CastSpell(_W)
	end
end
function getMyTrueRange()
	return myHero.range + GetDistance(myHero, myHero.minBBox)
end
function PluginOnCreateObj(obj)
	if obj and (obj.name:find("JaxDodger.troy") ~= nil) then
		CounterStrike = true
		--PrintChat("True")
	end
end
function PluginOnDeleteObj(obj)
	if obj and (obj.name:find("JaxDodger.troy") ~= nil) then
		CounterStrike = false
		--PrintChat("False")
	end
end
function PluginOnDraw()
	if Menu.Draw then
		if Menu.DrawQ then
		DrawCircle(myHero.x, myHero.y, myHero.z, qRange, 0x7F006E)
		end
		if Menu.DrawW then
		DrawCircle(myHero.x, myHero.y, myHero.z, wRange, 0x960084)
		end
		if Menu.DrawE then
		DrawCircle(myHero.x, myHero.y, myHero.z, eRange, 0x490041)
		end
		if Menu.DrawR then
		DrawCircle(myHero.x, myHero.y, myHero.z, rRange, 0xC900B1)
		end
		if Menu.DrawT and Target then
		DrawCircle(Target.x, Target.y, Target.z, 100, 0x7F006E)
		end
	end
end