function OnLoad()
menu = scriptConfig("Arrow Key Movement", "Alt Mover")
menu:addParam("MoveW", "Move W", SCRIPT_PARAM_ONKEYDOWN, false, 37)
menu:addParam("MoveN", "Move N", SCRIPT_PARAM_ONKEYDOWN, false, 38)
menu:addParam("MoveE", "Move E", SCRIPT_PARAM_ONKEYDOWN, false, 39)
menu:addParam("MoveS", "Move S", SCRIPT_PARAM_ONKEYDOWN, false, 40)
end
function OnTick()
SingleKeyMovement()
DoubleKeyMovement()
end
function SingleKeyMovement()
if menu.MoveW and not menu.MoveN and not menu.MoveE and not menu.MoveS then
myHero:MoveTo((myHero.x-150), (myHero.z+0))
end
if menu.MoveN and not menu.MoveW and not menu.MoveE and not menu.MoveS then
myHero:MoveTo((myHero.x+0), (myHero.z+150))
end
if menu.MoveE and not menu.MoveN and not menu.MoveW and not menu.MoveS then
myHero:MoveTo((myHero.x+150), (myHero.z+0))
end
if menu.MoveS and not menu.MoveN and not menu.MoveE and not menu.MoveW then
myHero:MoveTo((myHero.x+0), (myHero.z-150))
end
end
function DoubleKeyMovement()
if menu.MoveN and menu.MoveW and not menu.MoveE and not menu.MoveS then
myHero:MoveTo((myHero.x-150), (myHero.z+150))
end
if menu.MoveW and menu.MoveS and not menu.MoveE and not menu.MoveN then
myHero:MoveTo((myHero.x-150), (myHero.z-150))
end
if menu.MoveE and menu.MoveS and not menu.MoveW and not menu.MoveN then
myHero:MoveTo((myHero.x+150), (myHero.z-150))
end
if menu.MoveE and menu.MoveN and not menu.MoveW and not menu.MoveS then
myHero:MoveTo((myHero.x+150), (myHero.z+150))
end
end